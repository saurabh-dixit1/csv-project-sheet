# 𝐂𝐒𝐕 𝐅𝐢𝐥𝐞 𝐑𝐞𝐚𝐝𝐞𝐫 🚀

<br/>
<p align="center">
<img src=" https://raw.githubusercontent.com/Saurabhdixit93/SOCIALMEDIA/Temp-files/uploads/users_files/profile_pictures/IMG_20230401_130253_071.jpg">
</p>

## ⭐ Introduction

The CSV File Reader is a Full Stack Application built using MongoDB, ExpressJS, NodeJS, passport, nodemailer, EJS, Express Partials-Layouts,
Users are able to upload, read, delete the files from the application.

This website is completely Responsive across all the Devices.

-  Users can Upload Files in CSV format.
-  Uploaded Files are displayed in the centre
-  Front End & Back End Validation for uploading CSV Files less than 5MB.
-  The CSV Parser Reads the file and displays it in a Tabular format.
-  Users Receive Notifications for Deleting, Uploading the Files.
-  Users can Delete the uploaded Files.
-  Users can Search for the Keywords for a particular selected column in the Table.
-  Users can Sort the selected column in the Table in ASC/DESC order upon clicking the headings.
-  Users can navigate through the records using the Pagination which displays 100 records per page.
-  Users can view Column Chart & Pie Chart for a selected column in the Table, just below the Table.
And extra features user can create account and use and also option to use this app without having account.

   <br/>
      <br/>

      ## 🔥 Getting Started With The Project

      -  Fork the Project in your Repository.
      -  Clone the Forked Repository in your Local System.
      -  Install & Configure - NodeJS, MongoDB, Robo3T, POSTMAN.
      -  Create '.env' file & Set the Environment Variables in it, 
         DEPLOYMENT=production and local both<br/>
            DEPLOYMENT=local OR other OR Heroku OR AWS
            -  Run 'npm start' in GitBash Terminal
            -  Enjoy :)

            For any issues related to the project, raise an ISSUE in the respective Repository.
            <br/>
            <br/>

            ## 🔨 Tools Used

            <p align="justify">
            <img height="140" width="140" src="https://www.w3.org/html/logo/downloads/HTML5_Logo_256.png">
            <img height="140" width="140" src="https://logodix.com/logo/470309.png">
            <img height="140" width="140" src="https://upload.wikimedia.org/wikipedia/commons/6/6a/JavaScript-logo.png">
            <img height="140" width="140" src="https://upload.wikimedia.org/wikipedia/commons/b/b2/Bootstrap_logo.svg">
            <img height="140" width="140" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQv2l-4Y-ZVZm77rzV9CRJxmgNPpy36zgePIA&usqp=CAU">
            <img height="140" width="140" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSMX7p-_Zo1LqsEfO1v3B6Zw0Jgvhk4vo1fKA&usqp=CAU">
            <img height="140" width="140" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRASBParCnQhsRkKZ8opkkRjtk9XJ-MHdy0jA&usqp=CAU">
            <img height="140" width="140" src="https://code.visualstudio.com/assets/apple-touch-icon.png">
            </p>

            -  Library's 
            -  csv-parse
            -  dotenv
            -  ejs
            - nodemailer      
            -  express
            -  express-ejs-layouts
            -  mongoose
            - crypto
            -  morgan
            -  multer
            -  nodemon
            -  passport 
-  Framework: ExpressJS, Bootstrap
-  Database: MongoDB
-  Version Control System: Git
-  VCS Hosting: GitHub
-  Programming / Scripting: JavaScript
-  Front-End: CSC, EJS
-  Runtime Environment: NodeJS
-  Integrated Development Environment: VSCode
   <br/>
      <br/>

 <br/>

     ## Setup
   > - first download this Repository then open vs code and in Terminal write command 'npm Install'
     - then open it on 'localhost:4000/'
     - now you can start with this.


      ## 🔗 Links

   > ## Checkout the Website [Web Application](https://csv-file-reader-tuaw.onrender.com)



<br/>

## 💻 Screens

<p align="justify">
<img src="https://raw.githubusercontent.com/Saurabhdixit93/SOCIALMEDIA/Temp-files/uploads/users_files/profile_pictures/IMG_20230401_130253_071.jpg">
<img src="https://raw.githubusercontent.com/Saurabhdixit93/SOCIALMEDIA/Temp-files/uploads/users_files/profile_pictures/IMG_20230401_130415_652.jpg">
<img src="https://raw.githubusercontent.com/Saurabhdixit93/SOCIALMEDIA/Temp-files/uploads/users_files/profile_pictures/IMG_20230401_130416_351.jpg">
</p>
<br/>

## 🐦 Follow Me:

[LinkedIn](https://www.linkedin.com/in/saurabhdixit93)


I hope you like the project. Thanks for reading :)

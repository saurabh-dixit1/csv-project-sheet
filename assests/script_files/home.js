const fileInput = document.getElementById('uploaded_file');
const fileName = document.getElementById('filename');
fileInput.addEventListener('change', (event) =>{
    const selectedFile = event.target.files[0];
    fileName.textContent = selectedFile.name;
});
// Listen for file upload
fileInput.addEventListener('change', () => {
// Get the uploaded file
const file = fileInput.files[0];

// Check the file size
if (file.size > 10 * 1024 * 1024) {
alert('File size must be less than 10 MB.');
fileInput.value = ''; // clear the file input
fileName.textContent = '';
return;
}});

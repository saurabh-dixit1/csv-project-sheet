module.exports.setFlash = function(request , response , next){
    response.locals.flash = {
        'success': request.flash('success'),
        'error': request.flash('error')
    }
    next();
}


const path = require("path");
const fs = require("fs");

//Create Uploads Folder if it doesn't exist
module.exports.createUploads = async (req, res, next) => {
 try {
  let directory = path.join(__dirname, "..", "/uploads");
  if (!fs.existsSync(directory)) fs.mkdirSync(directory);
  directory = path.join(__dirname, "..", "/uploads/uploaded_files");
  if (!fs.existsSync(directory)) fs.mkdirSync(directory);
 } catch (error) {
  req.flash('error', 'ERROR:'+ error.message);
  console.log(error);
  return res.redirect('back');
 }
 next();
};

const mongoose = require('mongoose'); 

// Creating User Details Schema

const UserSchema = new mongoose.Schema({
    email: {
        type: String,
        required :true,
        unique : true
    },
    password: {
        type : String,
        required :true
    },
    name:{
        type :String,
        required :true
    },
   },
   {
    timestamps : true
});


//setup
const User = mongoose.model('User',UserSchema);
// Exports
module.exports = User;

module.exports.firstPage = function(req,res){
    if(req.isAuthenticated()){
        return res.redirect('/user');
    }

    
    return res.render('firstPage',{
        title:'Sign-Up & Log-in Page | CSV Reader',
        message: { type:null, text:null }
    });
}

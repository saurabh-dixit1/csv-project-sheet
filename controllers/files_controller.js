const multer = require('multer');   //Here multer used to upload files
const path = require('path');       //Importing path module
const csv = require('csv-parser');  //Here csv-parser used to convert data into json format
const fs = require('fs');           //importing fs to perform operations over file system
const uploadedFileNames = [];       //Creating array to contain names of uploaded files
const file_PATH = path.join(process.env.FILE_UPLOAD_PTH);// File Path


// //Module to set-up a multer storage
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname,'..' ,file_PATH));
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + '-' + path.extname(file.originalname);
    cb(null, file.originalname + '-' + uniqueSuffix +'.csv');
  }
});


  //Here file-filter function used to upload only (.csv) files
function fileFilter (req, file, cb) {

    if(file.mimetype == 'text/csv'){
        cb(null,true);
    }
    else{
      req.flash('error', 'Only .csv Files are Allowed. ');
      cb(null,false);
    }
  }
//file size 
  const upload = multer({storage:storage,limits:{fileSize:10 * 1024 * 1024},fileFilter:fileFilter}).single('uploaded_file');  //initializing multer

  
// //Module to upload file in uploads folder
module.exports.upload = (req,res) => {
    upload(req,res,function(err){
        if(err instanceof multer.MulterError){
          req.flash('error', 'Error: ' + err.message);
          res.redirect('back');
          return;
        }
        else if(err){
          req.flash('error', 'Error: ' + err.message);
          res.redirect('back');
          return;
        }
        else if(req.file){
          //store the file name in the array;
          uploadedFileNames.push(req.file.filename);
          req.flash('success' ,'File Uploaded SucccessFully');
          setTimeout(() => {
            const filePath = path.join(__dirname,'..' ,file_PATH , req.file.filename);
            fs.unlink(filePath ,function(err){
              if(err){console.log(err, 'error in settime');return}
              else{
              //remove the file name from  the array
              const index = uploadedFileNames.indexOf(req.file.filename);
              uploadedFileNames.splice(index , 1);
              }
            });
          }, 20 * 60 *1000); //auto delet in 20 min
        }
        return res.redirect('back');
    });
};

// //exporting array to make it accessible to other files
module.exports.uploadedFileNames = function(){
  return uploadedFileNames;
}
// //module to open the csv file and shows its content in a tabular form
module.exports.open = function(req,res){
  const csv_Parsed_Data = [];              //Creating array to store the data in JSON format
  const index = req.query.index;
  if (!isNaN(index) && index >= 0 && index < uploadedFileNames.length) {
  const filePath = path.join(__dirname,'..',file_PATH,uploadedFileNames[index]);
    fs.createReadStream(filePath) //setting up the path for file upload
    .pipe(csv())
    .on('data', (data) => csv_Parsed_Data.push(data))
    .on('end', () => {
    return res.render('main_view',{
          title: 'CSV  Reader | Tabuler View ',
          csvData: csv_Parsed_Data
      });
    });
  }else{
    req.flash('error' ,'Internal Server Error');
    return res.redirect('back');
  }
}

// //Module to delete any particular CSV file
module.exports.delete = function(req,res){
  let indx = req.query.index;
  try { var files = fs.readdirSync(path.join(__dirname,'..',file_PATH)); }
    catch(e) { return; }
    if (files.length > 0){
        var filePath = path.join(__dirname,'..',file_PATH,uploadedFileNames[indx]);
        if (fs.statSync(filePath).isFile())
          fs.unlinkSync(filePath);
    }
    uploadedFileNames.splice(indx,1);
    req.flash('success' , 'File SuccessFully Deleted');
    return res.redirect('back');
}

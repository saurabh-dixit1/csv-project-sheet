const express = require('express');
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
mongoose.set('strictQuery', false);
const dotenv = require('dotenv');
dotenv.config();
// app use express
const app = express();
// Set port num
const PORT = process.env.SERVER_PORT || 8000;
// DataBase Connection
const connectDB = async () => {
    try{
        const conn = await mongoose.connect(process.env.MONGO_URL);
        console.log(`MongoDB connected  successfull : ${conn.connection.host}`);
    }catch(err){
        console.log(`Error In Connecting MongoDB: ${err}`);
        process.exit(1);
    }
};
// url endcoding
app.use(express.urlencoded({ extended: true }));
// using cookie
app.use(cookieParser());
// Layouts setups
const expresslayouts = require('express-ejs-layouts');

// express-session
const session = require('express-session');
// for flash message require
const flash = require('connect-flash');
// custom middleware for flash
const customFlashMiddleWare = require('./configration/flash-middleware');
// Passport 
const passport = require('passport');
// local passport from configration
const passportLocal = require('./configration/passport-local-stretgy');
// google  stretgy
const passportGoogle = require('./configration/passport-google-oauth-stretgy');
// Requiring passport-jwt
const passport_JWT = require('passport-jwt');
const passport_JWT_Stretgy = require('./configration/passport-jwt-stretgy');

// permanent store cookie in storemongo using connect-mongo
const MongoStore = require('connect-mongo');
//static files uses
app.use(express.static(process.env.ASSETS_PATH));
//accesing uploaded files from uploads folder 
app.use('/uploads',express.static(__dirname + '/uploads')); // uploding files 
//layouts change
app.use(expresslayouts);
//extract styles and scripts from sub pages
app.set('layout extractStyles' , true);
app.set('layout extractScripts' , true);
// view engine 
app.set('view engine','ejs');  //setting up view engine as ejs
app.set('views',process.env.PROJECT_EJS_VIEW);   //setting path of views folder
// session use for cookies
app.use(session({
  name:process.env.PROJECT_SESSION_NAME,
  secret:process.env.SESSION_PRIVATE_COOKIE,
  saveUninitialized: false,
  resave: false,
  cookie:{
      maxAge:(1000 * 60 * 100)
  },
  // mongostore connection where to store the session cookies
  store: MongoStore.create( 
      { 
        mongoUrl : process.env.MONGO_URL,
        autoRemove: 'disabled'//I dont want to remove session cookies automatically
      },function(err){
        if(err){console.log('Error while trying to establish the connection and store session cookie:', err); return;}
        console.log('connect-mongo setup okay'); return;
    })
}));
// authentication uses 
app.use(passport.initialize());
app.use(passport.session());
app.use(passport.setAuthenticatedUser);

const nodemailer = require('nodemailer'); 
//Middleware - Creates Uploads Folder & Sub Folders, if not exists
app.use(customFlashMiddleWare.createUploads);
// // Using Flash 
app.use(flash());
app.use(customFlashMiddleWare.setFlash);
//Using Express Router For routing all access
app.use('/',require(process.env.PROJECT_ROUTERS));

// for production mode
connectDB().then(() => {
    app.listen(PORT, () =>{
        console.log(`Successfull Connected With the Port:${PORT}`);
    });
});
